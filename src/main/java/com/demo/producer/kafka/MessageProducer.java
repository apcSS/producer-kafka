package com.demo.producer.kafka;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

import com.example.demo.MsgRequest;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageProducer {
	private static final String MESSAGE_TOPIC_CUSTOMER = "customer.topic";

	@Autowired
	private KafkaTemplate<String, MsgRequest> template;

	public Map<String, Object> sendEmail(MsgRequest customer) {
		Map<String, Object> map = new HashMap();
		Date date = Calendar.getInstance(Locale.US).getTime();
		String code = "OK";
		ListenableFuture<SendResult<String, MsgRequest>> future = this.template.send(MESSAGE_TOPIC_CUSTOMER, customer);
		future.addCallback(new ListenableFutureCallback<SendResult<String, MsgRequest>>() {
			@Override
			public void onFailure(Throwable ex) {

				log.info("Unable to send message=[ {} ] due to : {}", customer.toString(), ex.getMessage());
			}

			@Override
			public void onSuccess(SendResult<String, MsgRequest> result) {
				log.info("Sent message=[ {} ] with offset=[ {} ]", customer.toString(),
						result.getRecordMetadata().offset());
			}
		});
		map.put("Code", code);
		map.put("Received_Time", date);
		return map;
	}
}
