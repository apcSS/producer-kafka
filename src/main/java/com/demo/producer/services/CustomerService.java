package com.demo.producer.services;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.demo.producer.kafka.MessageProducer;
import com.example.demo.MsgRequest;

@Service
public class CustomerService {
	@Autowired
	private MessageProducer sender;

	public ResponseEntity<Map<String, Object>> sendMessagetoKafka(MsgRequest reqeust) {
		Map<String, Object> map = sender.sendEmail(reqeust);
		return new ResponseEntity(map, HttpStatus.OK);
	}
}
