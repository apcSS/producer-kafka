package com.demo.producer.controllers;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.demo.producer.services.CustomerService;
import com.example.demo.MsgRequest;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/customers")
@Slf4j
public class CustomerController {
	  @Autowired
	    CustomerService customerService;

	    @PostMapping()
	    public ResponseEntity<Map<String, Object>> postCustomer(@RequestBody MsgRequest body) {
	    	log.info("post request >>>>>>>>>  : " + body.toString());
	        return  customerService.sendMessagetoKafka(body);
	    }
}
