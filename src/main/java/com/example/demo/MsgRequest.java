package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
public class MsgRequest {
	@JsonProperty("Msg_id")
	private Long msgId;

	@JsonProperty("Sender")

	private String sender;

	@JsonProperty("Msg")
	private String msg;

}
